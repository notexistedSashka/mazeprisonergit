﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Moving
{
    enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }

    class DirectionArg : EventArgs
    {
        public Direction Direction { get; set; }
    }
}
