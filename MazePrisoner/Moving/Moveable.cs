using MazePrisoner.Drawing;
using MazePrisoner.Units;

namespace MazePrisoner.Moving
{
    interface IMoveable
    {
        void Move(Direction direction);
    }
}
