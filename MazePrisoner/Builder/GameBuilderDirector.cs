﻿using MazePrisoner.Builder;
using MazePrisoner.Gaming;
using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Builder
{
    class GameBuilderDirector
    {
        private IGameBuilder gameBuilder;

        public GameBuilderDirector(IGameBuilder init_gameBuilder)
        {
            gameBuilder = init_gameBuilder;
        }

        public Game Construct(int width, int height)
        {
            gameBuilder.CreateMap(width, height);
            gameBuilder.CreateHero();
            gameBuilder.CreateEnemies();
            gameBuilder.CreateBoxes();
            gameBuilder.CreateDoor();

            return gameBuilder.GetResult();
        }
    }
}
