﻿using MazePrisoner.Drawing;
using MazePrisoner.Gaming;
using MazePrisoner.Units;
using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Builder
{
    interface IGameBuilder
    {
        void CreateMap(int width, int height);
        void CreateHero();
        void CreateEnemies();
        void CreateBoxes();
        void CreateDoor();

        Game GetResult();
    }
}
