﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using MazePrisoner.Gaming;

namespace MazePrisoner.WorkingWithFiles
{
    class FileManager
    {
        private const string fileName = "SavedGame.dat";

        static public void Save(Game game)
        {
            var formatter = new BinaryFormatter();

            using (var fileStream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            {
                formatter.Serialize(fileStream, game);
            }
        }

        static public Game Load()
        {
            var formatter = new BinaryFormatter();

            using (var fileStream = new FileStream(fileName, FileMode.Open))
            {
                return formatter.Deserialize(fileStream) as Game;
            }
        }
    }
}
