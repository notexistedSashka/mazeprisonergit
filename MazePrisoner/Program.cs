﻿using System;
using MazePrisoner.Menu;

namespace MazePrisoner
{
    class Program
    {
        static void Main(string[] args)
        {
            var menu = new MainMenu(30, 5, ConsoleColor.Yellow, ConsoleColor.DarkBlue);
            menu.Show();
            menu.Control();
        }
    }
}
