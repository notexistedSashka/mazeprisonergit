﻿using System;
using System.Collections.Generic;
using System.Text;
using MazePrisoner.Moving;
using MazePrisoner.Drawing;
using MazePrisoner.Shooting;
using System.Runtime.Serialization;

namespace MazePrisoner.Units
{
    [Serializable]
    class Unit : IMoveable, IDrawable, IDeserializationCallback
    {
        [NonSerialized]
        private EventHandler<DirectionArg> moveEvent;
        [NonSerialized]
        protected DirectionArg directionArg = new DirectionArg();

        public Direction MoveDirection { get; set; }

        public char Image { get; set; }
        public ConsoleColor FrontColor { get; set; }
        public Maze Maze { get; set; }
        public int CoordX { get; set; }
        public int CoordY { get; set; }
        public bool Visiable { get; set; }

        public Map Map { get; set; }

        // Запоминание клеток, которые прошел
        public IDrawable StepedNow { get; set; }

        public Unit(int x, int y, char image, ConsoleColor color, Map map)
        {
            MoveDirection = Direction.Right;
            Image = image;
            FrontColor = color;
            Maze = map.Maze;
            Map = map;
            CoordX = x;
            CoordY = y;
            Maze.MazeArr[CoordY, CoordX] = this;
            Visiable = false;

            FillMoveEvent();

            StepedNow = new Pass(CoordX, CoordY, Maze);
        }

        #region MoveEventMethods

        private void OnMoveEvent()
        {
            moveEvent?.Invoke(this, directionArg);
        }

        protected void FillMoveEvent()
        {
            moveEvent += (sender, args) =>
            {
                if (args.Direction == Direction.Up)
                {
                    if (Maze.MazeArr[CoordY - 1, CoordX].GetType().Name != "Wall")
                    {
                        StepedNow = Maze.MazeArr[CoordY - 1, CoordX];
                        CoordY--;
                    }
                }
            };

            moveEvent += (sender, args) =>
            {
                if (args.Direction == Direction.Down)
                {
                    if (Maze.MazeArr[CoordY + 1, CoordX].GetType().Name != "Wall")
                    {
                        StepedNow = Maze.MazeArr[CoordY + 1, CoordX];
                        CoordY++;
                    }
                }
            };

            moveEvent += (sender, args) =>
            {
                if (args.Direction == Direction.Left)
                {
                    if (Maze.MazeArr[CoordY, CoordX - 1].GetType().Name != "Wall")
                    {
                        StepedNow = Maze.MazeArr[CoordY, CoordX - 1];
                        CoordX--;
                    }
                }
            };

            moveEvent += (sender, args) =>
            {
                if (args.Direction == Direction.Right)
                {
                    if (Maze.MazeArr[CoordY, CoordX + 1].GetType().Name != "Wall")
                    {
                        StepedNow = Maze.MazeArr[CoordY, CoordX + 1];
                        CoordX++;
                    }
                }
            };
        }

        #endregion

        public void Move(Direction direction)
        {
            //Перебросить обязанность изменения координат в лабиринте на Game
            MoveDirection = direction;

            if (StepedNow is Unit)
                Maze.MazeArr[CoordY, CoordX] = BlockFactory.GetPass(CoordX, CoordX, Maze);
            else
                Maze.MazeArr[CoordY, CoordX] = StepedNow;

            if(StepedNow.Visiable)
                Map.DrawItem(CoordX, CoordY);

            directionArg.Direction = direction;
            OnMoveEvent();

            Maze.MazeArr[CoordY, CoordX] = this;

            if (Visiable)
                Map.DrawItem(CoordX, CoordY);
        }

        public bool CanContinueMove()
        {
            switch (MoveDirection)
            {
                case Direction.Up:
                    if (Maze.MazeArr[CoordY - 1, CoordX].GetType().Name == "Wall")
                        return false;
                    break;
                case Direction.Down:
                    if (Maze.MazeArr[CoordY + 1, CoordX].GetType().Name == "Wall")
                        return false;
                    break;
                case Direction.Left:
                    if (Maze.MazeArr[CoordY, CoordX - 1].GetType().Name == "Wall")
                        return false;
                    break;
                case Direction.Right:
                    if (Maze.MazeArr[CoordY, CoordX + 1].GetType().Name == "Wall")
                        return false;
                    break;
            }
            return true;
        }

        public void Draw()
        {
            Console.ForegroundColor = FrontColor;
            Console.Write(Image);
        }

        public void OnDeserialization(object sender)
        {
            directionArg = new DirectionArg();
            FillMoveEvent();
        }
    }

    class UnitFactory
    {
        public Unit CreateUnit(int x, int y, char image, ConsoleColor color, Map map) => new Unit(x, y, image, color, map);
    }
}
