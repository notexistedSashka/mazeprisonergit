﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using MazePrisoner.Drawing;
using MazePrisoner.Moving;
using MazePrisoner.Shooting;

namespace MazePrisoner.Units
{
    [Serializable]
    class Human : Unit, IShootable, IDeserializationCallback
    {
        [NonSerialized]
        private EventHandler<ShootArg> shootEvent;
        [NonSerialized]
        private ShootArg shootArg = new ShootArg();

        public int BulletsCount { get; set; } = 1;

        public Human(int x, int y, char image, ConsoleColor color, Map map) : base(x, y, image, color, map)
        {
            FillShootEvent();
        }

        #region ShootEventMethods

        private void OnShootEvent()
        {
            shootEvent?.Invoke(this, shootArg);
        }

        private void FillShootEvent()
        {
            shootEvent += (sender, args) =>
            {
                if (args.Direction == Direction.Up)
                {
                    if (Maze.MazeArr[CoordY - 1, CoordX].GetType().Name != "Wall")
                    {
                        args.X = CoordX;
                        args.Y = CoordY - 1;
                    }
                }
            };

            shootEvent += (sender, args) =>
            {
                if (args.Direction == Direction.Down)
                {
                    if (Maze.MazeArr[CoordY + 1, CoordX].GetType().Name != "Wall")
                    {
                        args.X = CoordX;
                        args.Y = CoordY + 1;
                    }
                }
            };

            shootEvent += (sender, args) =>
            {
                if (args.Direction == Direction.Left)
                {
                    if (Maze.MazeArr[CoordY, CoordX - 1].GetType().Name != "Wall")
                    {
                        args.X = CoordX - 1;
                        args.Y = CoordY;
                    }
                }
            };

            shootEvent += (sender, args) =>
            {
                if (args.Direction == Direction.Right)
                {
                    if (Maze.MazeArr[CoordY, CoordX + 1].GetType().Name != "Wall")
                    {
                        args.X = CoordX + 1;
                        args.Y = CoordY;
                    }
                }
            };
        }

        #endregion

        public Bullet Shoot()
        {
            var bulletFactory = new BulletFactory();

            shootArg.X = CoordX;
            shootArg.Y = CoordY;
            shootArg.Direction = MoveDirection;

            OnShootEvent();

            Maze.MazeArr[shootArg.Y, shootArg.X] = bulletFactory.CreateBullet(shootArg.X, shootArg.Y, '*', ConsoleColor.Red, Map, Maze.MazeArr[shootArg.Y, shootArg.X], MoveDirection);

            BulletsCount--;

            return Maze.MazeArr[shootArg.Y, shootArg.X] as Bullet;
        }

        public new void OnDeserialization(object sender)
        {
            directionArg = new DirectionArg();
            FillMoveEvent();
            shootArg = new ShootArg();
            FillShootEvent();
        }
    }
    class HumanFactory
    {
        public Human CreateHuman(int x, int y, char image, ConsoleColor color, Map map) => new Human(x, y, image, color, map);
    }
}
