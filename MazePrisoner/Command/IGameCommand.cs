﻿using MazePrisoner.Gaming;
using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Command
{
    abstract class IGameCommand
    {
        protected Game game;

        public IGameCommand(Game init_game) => game = init_game;

        public abstract void Execute();
    }
}
