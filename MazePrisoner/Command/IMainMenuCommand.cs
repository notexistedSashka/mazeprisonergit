﻿using MazePrisoner.Gaming;
using MazePrisoner.Menu;
using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Command
{
    abstract class IMainMenuCommand
    {
        protected MainMenu menu;

        public IMainMenuCommand(MainMenu init_menu) => menu = init_menu;

        public abstract void Execute();
    }
}
