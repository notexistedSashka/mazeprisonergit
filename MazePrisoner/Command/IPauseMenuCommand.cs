﻿using MazePrisoner.Menu;
using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Command
{

    abstract class IPauseMenuCommand
    {
        protected PauseMenu menu;

        public IPauseMenuCommand(PauseMenu init_menu) => menu = init_menu;

        public abstract void Execute();
    }
}
