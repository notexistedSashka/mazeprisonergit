﻿using System;
using MazePrisoner.Drawing;
using MazePrisoner.Moving;
using MazePrisoner.Units;
using System.Collections.Generic;
using System.Threading;
using MazePrisoner.Shooting;
using MazePrisoner.Menu;
using MazePrisoner.Command;
using MazePrisoner.Builder;

namespace MazePrisoner.Gaming
{
    [Serializable]
    class Game
    {
        public int KeysToEnterDoor { get; set; }

        public Map Map { get; set; }

        public Human Hero { get; set; }
        public List<Unit> Enemies { get; set; }
        public List<IDrawable> Boxes { get; set; }
        public List<Bullet> Bullets { get; set; }
        public Door Door { get; set; }

        public bool Continue { get; set; }

        private int keys = 0;

        public Game() { }

        public Game(int mapWidth, int mapHeight)
        {
            var gameBuilderDirector = new GameBuilderDirector(new GameBuilder(this));
            gameBuilderDirector.Construct(mapWidth, mapHeight);
            Bullets = new List<Bullet>();
        }

        public void DrawMaze()
        {
            Map.DrawMaze();
        }

        public Direction RandomDirection()
        {
            var rand = new Random();
            switch (rand.Next(0, 4))
            {
                case 0:
                    return Direction.Down;
                case 1:
                    return Direction.Up;
                case 2:
                    return Direction.Left;
                default:
                    return Direction.Right;
            }
        }

        public int Play()
        {
            var dictionary = new Dictionary<ConsoleKey, IGameCommand>
            {
                { ConsoleKey.Spacebar, new SpacebarCommand(this) },
                { ConsoleKey.UpArrow, new UpArrowCommand(this) },
                { ConsoleKey.DownArrow, new DownArrowCommand(this) },
                { ConsoleKey.LeftArrow, new LeftArrowCommand(this) },
                { ConsoleKey.RightArrow, new RightArrowCommand(this) },
                { ConsoleKey.Escape, new EscapeCommand(this) }
            };

            // Свой таймер
            var enemyTimer = new DateTime();
            var bulletTimer = new DateTime();

            Console.SetWindowSize(Map.Maze.MapWidth + 1, Map.Maze.MapHeight + 2);

            Map.DrawAroundUnit(Hero);
            Console.CursorVisible = false;


            ConsoleKeyInfo key;
            while (true)
            {
                ShowInfoBar();

                if (Console.KeyAvailable)
                {
                    key = Console.ReadKey(true);

                    dictionary[key.Key].Execute();

                    if (key.Key == ConsoleKey.Escape)
                    {
                        if (Continue)
                        {
                            Console.Clear();
                            Map.DrawMaze();
                        }
                        else
                        {
                            return 0;
                        }
                    }
                }

                RemoveEnemies();
                PickUpBox();

                if (HasLost()) return -1;
                if (HasWon()) return 1;

                if (DateTime.Now - enemyTimer > TimeSpan.FromMilliseconds(300))
                {
                    MoveEnemies();
                    enemyTimer = DateTime.Now;
                }

                if (DateTime.Now - bulletTimer > TimeSpan.FromMilliseconds(100))
                {
                    MoveBullets();
                    bulletTimer = DateTime.Now;
                }

                Map.DrawAroundUnit(Hero);
            }
        }

        private void RemoveEnemies()
        {
            bool killed = false;
            foreach (var item in Enemies)
            {
                foreach (var bullet in Bullets)
                {
                    if (item.CoordX == bullet.CoordX && item.CoordY == bullet.CoordY)
                    {
                        Enemies.Remove(item);
                        Bullets.Remove(bullet);
                        Map.Maze.MazeArr[item.CoordY, item.CoordX] = BlockFactory.GetPass(item.CoordX, item.CoordY, Map.Maze);
                        killed = true;
                        break;
                    }
                }
                if (killed)
                    break;
            }
        }

        private void PickUpBox()
        {
            var rand = new Random();
            foreach (var item in Boxes)
            {
                if (item.CoordX == Hero.CoordX && item.CoordY == Hero.CoordY)
                {
                    if (Boxes.Count == KeysToEnterDoor - keys)
                        keys++;
                    else if (rand.Next(0, 100) < 25 && keys < KeysToEnterDoor)
                        keys++;
                    else
                        Hero.BulletsCount++;

                    Hero.StepedNow = BlockFactory.GetPass(item.CoordX, item.CoordY, Map.Maze);
                    Boxes.Remove(item);
                    break;
                }
            }
        }

        private void ShowInfoBar()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.SetCursorPosition(0, Map.Maze.MapHeight + 1);
            Console.Write($"Keys: {keys}");

            Console.SetCursorPosition(15, Map.Maze.MapHeight + 1);
            Console.Write($"Bullets: {Hero.BulletsCount}");

            Console.SetCursorPosition(28, Map.Maze.MapHeight + 1);
            Console.Write($"Enemies: {Enemies.Count}");

            Console.SetCursorPosition(40, Map.Maze.MapHeight + 1);
            Console.Write($"Keys to open door: {KeysToEnterDoor}");
        }

        private bool HasLost()
        {
            foreach (var item in Enemies)
            {
                if (item.CoordX == Hero.CoordX && item.CoordY == Hero.CoordY)
                    return true;
            }
            return false;
        }

        private bool HasWon()
        {
            if (Door.CoordX == Hero.CoordX && Door.CoordY == Hero.CoordY && keys == KeysToEnterDoor)
            {
                return true;
            }
            return false;
        }

        private void MoveEnemies()
        {
            foreach (var item in Enemies)
            {
                if (!item.CanContinueMove())
                    item.MoveDirection = RandomDirection();
                item.Move(item.MoveDirection);
            }
        }

        private void MoveBullets()
        {
            foreach (var item in Bullets)
            {
                if (item.CanContinueMove())
                {
                    item.Move(item.MoveDirection);
                }
                else
                {
                    Map.Maze.MazeArr[item.CoordY, item.CoordX] = BlockFactory.GetPass(item.CoordX, item.CoordY, Map.Maze);
                    Bullets.Remove(item);
                    break;
                }
            }
        }

        #region PlayCommandsMethods

        public void Pause()
        {
            var pauseMenu = PauseMenu.GetPauseMenu(30, 4, ConsoleColor.Yellow, ConsoleColor.DarkBlue);
            pauseMenu.Show();
            pauseMenu.Control();
            Continue = pauseMenu.Continue;
        }

        public void HeroShoot()
        {
            if (Hero.BulletsCount > 0 && Hero.CanContinueMove())
                Bullets.Add(Hero.Shoot());
        }

        public void HeroMoveUp() => Hero.Move(Direction.Up);

        public void HeroMoveDown() => Hero.Move(Direction.Down);

        public void HeroMoveRight() => Hero.Move(Direction.Right);

        public void HeroMoveLeft() => Hero.Move(Direction.Left);

        #endregion
    }
}
