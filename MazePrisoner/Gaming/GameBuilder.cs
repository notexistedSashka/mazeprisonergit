﻿using MazePrisoner.Builder;
using MazePrisoner.Drawing;
using MazePrisoner.Units;
using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Gaming
{
    class GameBuilder : IGameBuilder
    {
        private Game game;

        public GameBuilder(Game init_game)
        {
            game = init_game;
        }

        public Game GetResult() => game;

        public void CreateBoxes()
        {
            var boxes = new List<IDrawable>();

            IDrawable pass;
            for (int i = 0; i < game.Map.Maze.MapHeight * game.Map.Maze.MapWidth / 300; i++)
            {
                pass = game.Map.Maze.GetPass();
                boxes.Add(BlockFactory.GetBox(pass.CoordX, pass.CoordY, game.Map.Maze));
            }
            game.KeysToEnterDoor = game.Map.Maze.MapHeight * game.Map.Maze.MapWidth / 300 / 2;

            game.Boxes = boxes;
        }

        public void CreateDoor()
        {
            IDrawable pass = game.Map.Maze.GetPass();
            game.Door = BlockFactory.GetDoor(pass.CoordX, pass.CoordY, game.Map.Maze) as Door;
        }

        public void CreateEnemies()
        {
            var enemies = new List<Unit>();

            var unitFactory = new UnitFactory();

            IDrawable pass;
            for (int i = 0; i < game.Map.Maze.MapHeight * game.Map.Maze.MapWidth / 200; i++)
            {
                pass = game.Map.Maze.GetPass();
                enemies.Add(unitFactory.CreateUnit(pass.CoordX, pass.CoordY, 'o', ConsoleColor.Red, game.Map));
            }

            foreach (var item in enemies)
            {
                item.MoveDirection = game.RandomDirection();
            }

            game.Enemies = enemies;
        }

        public void CreateHero()
        {
            var heroFactory = new HumanFactory();
            IDrawable pass = game.Map.Maze.GetPass();
            var h = heroFactory.CreateHuman(pass.CoordX, pass.CoordY, 'o', ConsoleColor.Green, game.Map);
            h.Visiable = true;
            game.Hero = h;
        }

        public void CreateMap(int width, int height)
        {
            var map = new Map(width, height);
            game.Map = map;
        }
    }
}
