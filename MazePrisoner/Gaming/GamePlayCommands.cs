﻿using MazePrisoner.Command;
using MazePrisoner.Menu;
using MazePrisoner.Moving;
using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Gaming
{
    class SpacebarCommand : IGameCommand
    {
        public SpacebarCommand(Game init_game) : base(init_game) { }
        public override void Execute() => game.HeroShoot();
    }

    class UpArrowCommand : IGameCommand
    {
        public UpArrowCommand(Game init_game) : base(init_game) { }
        public override void Execute() => game.HeroMoveUp();
    }

    class DownArrowCommand : IGameCommand
    {
        public DownArrowCommand(Game init_game) : base(init_game) { }
        public override void Execute() => game.HeroMoveDown();
    }

    class LeftArrowCommand : IGameCommand
    {
        public LeftArrowCommand(Game init_game) : base(init_game) { }
        public override void Execute() => game.HeroMoveLeft();
    }

    class RightArrowCommand : IGameCommand
    {
        public RightArrowCommand(Game init_game) : base(init_game) { }
        public override void Execute() => game.HeroMoveRight();
    }

    class EscapeCommand : IGameCommand
    {
        public EscapeCommand(Game init_game) : base(init_game) { }
        public override void Execute() => game.Pause();
    }
}
