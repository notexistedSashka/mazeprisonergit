﻿using MazePrisoner.Units;
using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Drawing
{
    [Serializable]
    class Maze
    {
        public int MapHeight { get; private set; }
        public int MapWidth { get; private set; }
        public IDrawable[,] MazeArr { get; set; }

        public Maze(int width, int height)
        {
            MapWidth = width;
            MapHeight = height;
            MazeArr = new IDrawable[MapHeight, MapWidth];
            CreateMaze();
        }

        #region MethodsForCreatingMaze

        private List<IDrawable> GetNeighbours(Pass startCell)
        {
            List<IDrawable> passString = new List<IDrawable>();

            if (startCell.CoordX + 2 < MapWidth)
                if (MazeArr[startCell.CoordY, startCell.CoordX + 2].GetType().Name == "Pass"
                    && MazeArr[startCell.CoordY, startCell.CoordX + 1].GetType().Name == "Wall"
                    && MazeArr[startCell.CoordY, startCell.CoordX + 3].GetType().Name == "Wall"
                    && MazeArr[startCell.CoordY + 1, startCell.CoordX + 2].GetType().Name == "Wall"
                    && MazeArr[startCell.CoordY - 1, startCell.CoordX + 2].GetType().Name == "Wall")
                    passString.Add(MazeArr[startCell.CoordY, startCell.CoordX + 2]);
            if (startCell.CoordX - 2 > 0)
                if (MazeArr[startCell.CoordY, startCell.CoordX - 2].GetType().Name == "Pass"
                    && MazeArr[startCell.CoordY, startCell.CoordX - 1].GetType().Name == "Wall"
                    && MazeArr[startCell.CoordY, startCell.CoordX - 3].GetType().Name == "Wall"
                    && MazeArr[startCell.CoordY + 1, startCell.CoordX - 2].GetType().Name == "Wall"
                    && MazeArr[startCell.CoordY - 1, startCell.CoordX - 2].GetType().Name == "Wall")
                    passString.Add(MazeArr[startCell.CoordY, startCell.CoordX - 2]);
            if (startCell.CoordY + 2 < MapHeight)
                if (MazeArr[startCell.CoordY + 2, startCell.CoordX].GetType().Name == "Pass"
                    && MazeArr[startCell.CoordY + 1, startCell.CoordX].GetType().Name == "Wall"
                    && MazeArr[startCell.CoordY + 3, startCell.CoordX].GetType().Name == "Wall"
                    && MazeArr[startCell.CoordY + 2, startCell.CoordX - 1].GetType().Name == "Wall"
                    && MazeArr[startCell.CoordY + 2, startCell.CoordX + 1].GetType().Name == "Wall")
                    passString.Add(MazeArr[startCell.CoordY + 2, startCell.CoordX]);
            if (startCell.CoordY - 2 > 0)
                if (MazeArr[startCell.CoordY - 2, startCell.CoordX].GetType().Name == "Pass"
                    && MazeArr[startCell.CoordY - 1, startCell.CoordX].GetType().Name == "Wall"
                    && MazeArr[startCell.CoordY - 3, startCell.CoordX].GetType().Name == "Wall"
                    && MazeArr[startCell.CoordY - 2, startCell.CoordX - 1].GetType().Name == "Wall"
                    && MazeArr[startCell.CoordY - 2, startCell.CoordX + 1].GetType().Name == "Wall")
                    passString.Add(MazeArr[startCell.CoordY - 2, startCell.CoordX]);

            return passString;
        }

        private void RemoveWall(IBlock currentPass, IDrawable neighbour)
        {
            if (currentPass.CoordX == neighbour.CoordX)
                MazeArr[(currentPass.CoordY + neighbour.CoordY) / 2, currentPass.CoordX] = BlockFactory.GetPass(currentPass.CoordX, (currentPass.CoordY + neighbour.CoordY) / 2, this);
            else
                MazeArr[currentPass.CoordY, (currentPass.CoordX + neighbour.CoordX) / 2] = BlockFactory.GetPass((currentPass.CoordX + neighbour.CoordX) / 2, currentPass.CoordY, this);
        }

        #endregion

        public void CreateMaze()
        {
            List<IDrawable> unvisited = new List<IDrawable>();

            for (int i = 0; i < MapHeight; i++)
            {
                for (int j = 0; j < MapWidth; j++)
                {
                    if ((i % 2 != 0 && j % 2 != 0) && (i < MapHeight - 1 && j < MapWidth - 1))
                    {
                        MazeArr[i, j] = BlockFactory.GetPass(j, i, this);
                        unvisited.Add(MazeArr[i, j]);
                    }
                    else
                    {
                        MazeArr[i, j] = BlockFactory.GetWall(j, i, this);
                    }
                }
            }

            var currentCell = BlockFactory.GetPass(1, 1, this);
            IDrawable neighbourCell;

            var rand = new Random();

            var stack = new Stack<Pass>();

            int unvisitedCount = unvisited.Count - 1;

            do
            {
                var Neighbours = GetNeighbours(currentCell);
                if (Neighbours.Count != 0)
                {
                    neighbourCell = Neighbours[rand.Next(0, Neighbours.Count)];
                    stack.Push(currentCell);
                    RemoveWall(currentCell, neighbourCell);
                    currentCell = neighbourCell as Pass;
                    unvisitedCount--;
                }
                else if (stack.Count > 0)
                {
                    currentCell = stack.Pop();
                }
                else
                {
                    currentCell = unvisited[rand.Next(0, unvisited.Count - 1)] as Pass;
                    unvisitedCount--;
                }
            }
            while (unvisitedCount > 0);
        }

        public IBlock GetPass()
        {
            var rand = new Random();
            var tmp = MazeArr[rand.Next(0, MapHeight), rand.Next(0, MapWidth)];

            while (tmp.GetType().Name != "Pass")
                tmp = MazeArr[rand.Next(0, MapHeight), rand.Next(0, MapWidth)];

            return tmp as IBlock;
        }
    }

    class MazeFactory
    {
        public Maze CreateMaze(int width, int height) => new Maze(width, height);
    }
}
