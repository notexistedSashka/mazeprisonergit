﻿using System;

namespace MazePrisoner.Drawing
{
    interface IDrawable
    {
        char Image { get; set; }
        ConsoleColor FrontColor { get; set; }
        Maze Maze { get; set; }
        int CoordX { get; set; }
        int CoordY { get; set; }
        bool Visiable { get; set; }

        void Draw();
    }
}
