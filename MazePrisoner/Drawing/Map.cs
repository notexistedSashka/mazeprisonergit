﻿using System;
using System.Collections.Generic;
using System.Text;
using MazePrisoner.Units;

namespace MazePrisoner.Drawing
{
    [Serializable]
    class Map
    {
        public Maze Maze { get; set; }

        public Map(int width, int height)
        {
            var mazeFactory = new MazeFactory();
            Maze = mazeFactory.CreateMaze(width, height);
        }

        public void DrawMaze()
        {
            for (int i = 0; i < Maze.MapHeight; i++)
            {
                for (int j = 0; j < Maze.MapWidth; j++)
                {
                    if (Maze.MazeArr[i, j].Visiable)
                        DrawItem(j, i);
                }
            }
        }

        public void DrawAroundUnit(Unit unit)
        {
            for (int i = unit.CoordY - 3; i < unit.CoordY + 3; i++)
            {
                for (int j = unit.CoordX - 3; j < unit.CoordX + 3; j++)
                {
                    if (i >= Maze.MapHeight || i < 0)
                        break;
                    else if (j < Maze.MapWidth && j >= 0)
                    {
                        if (Maze.MazeArr[i, j] is IBlock)
                            Maze.MazeArr[i, j].Visiable = true;
                        DrawItem(j, i);
                    }
                }
            }
        }

        public void DrawItem(int x, int y)
        {
            Console.SetCursorPosition(x, y);
            Maze.MazeArr[y, x].Draw();
        }
    }
}
