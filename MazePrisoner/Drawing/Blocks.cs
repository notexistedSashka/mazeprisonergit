﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Drawing
{
    interface IBlock : IDrawable
    {
    }

    [Serializable]
    class Pass : IBlock
    {
        public char Image { get; set; }
        public ConsoleColor FrontColor { get; set; }
        public Maze Maze { get; set; }
        public int CoordX { get; set; }
        public int CoordY { get; set; }
        public bool Visiable { get; set; }

        public Pass(int x, int y, Maze maze)
        {
            Image = ' ';
            FrontColor = ConsoleColor.Black;
            Maze = maze;
            CoordX = x;
            CoordY = y;
            Visiable = false;
        }

        public void Draw()
        {
            Console.ForegroundColor = FrontColor;
            Console.Write(Image);
        }
    }

    [Serializable]
    class Wall : IBlock
    {
        public char Image { get; set; }
        public ConsoleColor FrontColor { get; set; }
        public Maze Maze { get; set; }
        public int CoordX { get; set; }
        public int CoordY { get; set; }
        public bool Visiable { get; set; }

        public Wall(int x, int y, Maze maze)
        {
            Image = '█';
            FrontColor = ConsoleColor.DarkYellow;
            Maze = maze;
            CoordX = x;
            CoordY = y;
            Visiable = false;
        }

        public void Draw()
        {
            Console.ForegroundColor = FrontColor;
            Console.Write(Image);
        }
    }

    [Serializable]
    class Box : IBlock
    {
        public char Image { get; set; }
        public ConsoleColor FrontColor { get; set; }
        public Maze Maze { get; set; }
        public int CoordX { get; set; }
        public int CoordY { get; set; }
        public bool Visiable { get; set; }

        public Box(int x, int y, Maze maze)
        {
            Image = '█';
            FrontColor = ConsoleColor.Blue;
            Maze = maze;
            CoordX = x;
            CoordY = y;
            Maze.MazeArr[CoordY, CoordX] = this;
            Visiable = false;
        }

        public void Draw()
        {
            Console.ForegroundColor = FrontColor;
            Console.Write(Image);
        }
    }

    [Serializable]
    class Door : IBlock
    {
        public char Image { get; set; }
        public ConsoleColor FrontColor { get; set; }
        public Maze Maze { get; set; }
        public int CoordX { get; set; }
        public int CoordY { get; set; }
        public bool Visiable { get; set; }

        public Door(int x, int y, Maze maze)
        {
            Image = '█';
            FrontColor = ConsoleColor.DarkGreen;
            Maze = maze;
            CoordX = x;
            CoordY = y;
            Maze.MazeArr[CoordY, CoordX] = this;
            Visiable = false;
        }

        public void Draw()
        {
            Console.ForegroundColor = FrontColor;
            Console.Write(Image);
        }
    }

    [Serializable]
    class BlockFactory
    {
        static public Pass GetPass(int x, int y, Maze Maze) => new Pass(x, y, Maze);
        static public Wall GetWall(int x, int y, Maze Maze) => new Wall(x, y, Maze);
        static public Box GetBox(int x, int y, Maze Maze) => new Box(x, y, Maze);
        static public Door GetDoor(int x, int y, Maze Maze) => new Door(x, y, Maze);
    }
}
