﻿using System;
using System.Collections.Generic;
using System.Text;
using MazePrisoner.Moving;

namespace MazePrisoner.Shooting
{
    class ShootArg
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Direction Direction { get; set; }
    }
}
