﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Shooting
{
    interface IShootable
    {
        public int BulletsCount { get; set; }
        public Bullet Shoot();
    }
}
