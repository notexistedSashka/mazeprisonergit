﻿using System;
using System.Collections.Generic;
using System.Text;
using MazePrisoner.Drawing;
using MazePrisoner.Moving;
using MazePrisoner.Units;

namespace MazePrisoner.Shooting
{
    [Serializable]
    class Bullet : Unit
    {
        public Bullet(int x, int y, char image, ConsoleColor color, Map map, IDrawable steped, Direction direction) : base(x, y, image, color, map)
        {
            StepedNow = steped;
            MoveDirection = direction;
        }
        public void Fly() => Move(MoveDirection);
    }

    class BulletFactory
    {
        public Bullet CreateBullet(int x, int y, char image, ConsoleColor color, Map map, IDrawable steped, Direction direction) => new Bullet(x, y, image, color, map, steped, direction);
    }
}
