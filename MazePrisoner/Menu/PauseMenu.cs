﻿using MazePrisoner.Command;
using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Menu
{
    class PauseMenu : Menu
    {
        static PauseMenu pauseMenuInstance = null;

        public bool Continue { get; set; } = true;

        public PauseMenu(int width, int height, ConsoleColor frontColor, ConsoleColor backColor) : base(width, height, frontColor, backColor)
        {
            MenuItems = new string[] { "Continue", "Back to main menu" };
        }

        public static PauseMenu GetPauseMenu(int width, int height, ConsoleColor frontColor, ConsoleColor backColor)
        {
            if (pauseMenuInstance == null)
                pauseMenuInstance = new PauseMenu(width, height, frontColor, backColor);

            return pauseMenuInstance;
        }

        protected override void MakeCurrentItemAct()
        {
            var dictionary = new Dictionary<string, IPauseMenuCommand>
            {
                { MenuItems[0], new ContinueCommand(this) },
                { MenuItems[1], new BackToMainMenuCommand(this) }
            };

            dictionary[MenuItems[currentItem]].Execute();
        }
        public override void Control()
        {
            while (true)
            {
                var key = Console.ReadKey(true);
                //Избавиться от свича
                switch (key.Key)
                {
                    case ConsoleKey.Enter:
                        MakeCurrentItemAct();
                        return;
                    case ConsoleKey.UpArrow:
                        ChangeCurrentItemBackColor(BackColor);
                        currentItem = currentItem > 0 ? currentItem - 1 : MenuItems.Length - 1;
                        ChangeCurrentItemBackColor(ConsoleColor.Black);
                        break;
                    case ConsoleKey.DownArrow:
                        ChangeCurrentItemBackColor(BackColor);
                        currentItem = currentItem < MenuItems.Length - 1 ? currentItem + 1 : 0;
                        ChangeCurrentItemBackColor(ConsoleColor.Black);
                        break;
                }
            }
        }

        #region PauseMenuCommandsMethods

        public void ContinueGame() => Continue = true;

        public void BackToMainMenu() => Continue = false;

        #endregion
    }
}
