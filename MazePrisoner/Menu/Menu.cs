﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Menu
{
    abstract class Menu : IMenu
    {
        protected char[,] arr;
        protected int currentItem;

        public int Width { get; set; }
        public int Height { get; set; }
        public ConsoleColor FrontColor { get; set; }
        public ConsoleColor BackColor { get; set; }
        public string[] MenuItems { get; set; }

        public Menu(int width, int height, ConsoleColor frontColor, ConsoleColor backColor)
        {
            Width = width;
            Height = height;
            FrontColor = frontColor;
            BackColor = backColor;

            arr = new char[height, width];

            currentItem = 0;

            CreateForm();
        }

        public void Show()
        {
            Console.CursorVisible = false;

            Console.BackgroundColor = BackColor;
            Console.ForegroundColor = FrontColor;

            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    Console.SetCursorPosition(j + (Console.WindowWidth - Width) / 2 - 1, i + (Console.WindowHeight - Height) / 2 - 1);
                    Console.Write(arr[i, j]);
                }
            }

            DrawMenuItems();
            ChangeCurrentItemBackColor(ConsoleColor.Black);
        }

        virtual public void Control()
        {
            while (true)
            {
                var key = Console.ReadKey(true);

                //Избавиться от свича
                switch (key.Key)
                {
                    case ConsoleKey.Enter:
                        MakeCurrentItemAct();
                        break;
                    case ConsoleKey.UpArrow:
                        ChangeCurrentItemBackColor(BackColor);
                        currentItem = currentItem > 0 ? currentItem - 1 : MenuItems.Length - 1;
                        ChangeCurrentItemBackColor(ConsoleColor.Black);
                        break;
                    case ConsoleKey.DownArrow:
                        ChangeCurrentItemBackColor(BackColor);
                        currentItem = currentItem < MenuItems.Length - 1 ? currentItem + 1 : 0;
                        ChangeCurrentItemBackColor(ConsoleColor.Black);
                        break;
                }
            }
        }

        abstract protected void MakeCurrentItemAct();

        protected void ChangeCurrentItemBackColor(ConsoleColor color)
        {
            Console.BackgroundColor = color;
            for (int j = 1; j < Width - 1; j++)
            {
                Console.SetCursorPosition(j + (Console.WindowWidth - Width) / 2 - 1, (Console.WindowHeight - Height) / 2 + currentItem);
                Console.Write(arr[currentItem + 1, j]);
            }

            Console.SetCursorPosition((Console.WindowWidth - Width) / 2, (Console.WindowHeight - Height) / 2 + currentItem);
            Console.Write(MenuItems[currentItem]);
        }

        private void CreateForm()
        {
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    if (i == 0 && j == 0)
                        arr[i, j] = '┌';
                    else if (i == 0 && j == Width - 1)
                        arr[i, j] = '┐';
                    else if (i == Height - 1 && j == 0)
                        arr[i, j] = '└';
                    else if (i == Height - 1 && j == Width - 1)
                        arr[i, j] = '┘';
                    else if (j == 0 || j == Width - 1)
                        arr[i, j] = '│';
                    else if (i == 0 || i == Height - 1)
                        arr[i, j] = '─';
                    else
                        arr[i, j] = ' ';
                }
            }
        }

        private void DrawMenuItems()
        {
            for (int i = 1; i < Height - 1; i++)
            {
                Console.SetCursorPosition((Console.WindowWidth - Width) / 2, i + (Console.WindowHeight - Height) / 2 - 1);
                Console.Write(MenuItems[i - 1]);
            }
        }
    }
}

