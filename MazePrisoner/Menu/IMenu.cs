﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Menu
{
    interface IMenu
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public ConsoleColor FrontColor { get; set; }
        public ConsoleColor BackColor { get; set; }
        public string[] MenuItems { get; set; }

        public void Show();
    }
}
