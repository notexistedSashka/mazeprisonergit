﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MazePrisoner.Command;
using MazePrisoner.Gaming;
using MazePrisoner.WorkingWithFiles;

namespace MazePrisoner.Menu
{
    class NewGameCommand : IMainMenuCommand
    {
        public NewGameCommand(MainMenu init_menu) : base(init_menu) { }
        public override void Execute() => menu.NewGame();
    }

    class LoadCommand : IMainMenuCommand
    {
        public LoadCommand(MainMenu init_menu) : base(init_menu) { }
        public override void Execute() => menu.LoadGame();
    }

    class ExitCommand : IMainMenuCommand
    {
        public ExitCommand(MainMenu init_menu) : base(init_menu) { }
        public override void Execute() => menu.Exit();
    }
}
