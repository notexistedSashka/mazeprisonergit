﻿using MazePrisoner.Command;
using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Menu
{
    class ContinueCommand : IPauseMenuCommand
    {
        public ContinueCommand(PauseMenu init_menu) : base(init_menu) { }

        public override void Execute() => menu.ContinueGame();
    }

    class BackToMainMenuCommand : IPauseMenuCommand
    {
        public BackToMainMenuCommand(PauseMenu init_menu) : base(init_menu) { }

        public override void Execute() => menu.BackToMainMenu();
    }
}
