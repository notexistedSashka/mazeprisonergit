﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MazePrisoner.Menu
{
    class FinalMenu : Menu
    {
        public FinalMenu(int width, int height, ConsoleColor frontColor, ConsoleColor backColor) : base(width, height, frontColor, backColor)
        {
            MenuItems = new string[] { "Back to main menu" };
        }

        public override void Control()
        {
            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey(true);
            } while (key.Key != ConsoleKey.Enter);
        }

        protected override void MakeCurrentItemAct()
        {
            throw new NotImplementedException();
        }
    }
}
