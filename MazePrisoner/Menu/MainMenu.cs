﻿using MazePrisoner.Command;
using MazePrisoner.Gaming;
using MazePrisoner.WorkingWithFiles;
using System;
using System.Collections.Generic;
using System.IO;

namespace MazePrisoner.Menu
{
    class MainMenu : Menu
    {
        public Game game { get; set; }

        public MainMenu(int width, int height, ConsoleColor frontColor, ConsoleColor backColor) : base(width, height, frontColor, backColor)
        {
            MenuItems = new string[] { "New game", "Load", "Exit" };
            game = new Game(61, 13);
        }

        protected override void MakeCurrentItemAct()
        {
            var dictionary = new Dictionary<string, IMainMenuCommand>
            {
                { MenuItems[0], new NewGameCommand(this) },
                { MenuItems[1], new LoadCommand(this) },
                { MenuItems[2], new ExitCommand(this) }
            };

            try
            {
                dictionary[MenuItems[currentItem]].Execute();
            }
            catch (FileNotFoundException)
            {
                return;
            }
        }

        public void PlayGame(Game game)
        {
            var finalMenu = new FinalMenu(30, 3, ConsoleColor.Red, ConsoleColor.DarkBlue);

            int playCode = game.Play();
            if (playCode > 0)
            {
                finalMenu = new FinalMenu(30, 3, ConsoleColor.Green, ConsoleColor.DarkBlue);
            }
            else if (playCode == 0)
            {
                FileManager.Save(game);

                Console.Clear();
                Show();

                return;
            }

            Console.Clear();
            finalMenu.Show();
            finalMenu.Control();

            Console.Clear();
            Show();
        }

        #region MainMenuCommandsMethods

        public void NewGame()
        {
            Console.ResetColor();
            Console.Clear();

            PlayGame(game);
        }

        public void LoadGame()
        {
            try
            {
                game = FileManager.Load();
            }
            catch (FileNotFoundException)
            {
                throw;
            }
            Console.ResetColor();
            Console.Clear();
            game.DrawMaze();

            PlayGame(game);
        }

        public void Exit()
        {
            Console.ResetColor();
            Console.Clear();
            Environment.Exit(1);
        }

        #endregion
    }
}
